/*******************************************************************************
* Copyright (c) 2005, 2006 QFS GmbH. All rights reserved.
* The contents of this file are made available under the terms
* of the GNU Lesser General Public License (LGPL) Version 2.1 that
* accompanies this distribution (lgpl-v21.txt).  The LGPL is also
* available at http://www.gnu.org/licenses/lgpl.html.  If the version
* of the LGPL at http://www.gnu.org is different to the version of
* the LGPL accompanying this distribution and there is any conflict
* between the two license versions, the terms of the LGPL accompanying
* this distribution shall govern.
*
* Contributors:
 *     QFS GmbH - Accessibility/Testing hooks
*******************************************************************************/
#include "swt.h"
#include "qf_structs.h"
#include "qf_os.h"
#include "os_structs.h"
#include "os_stats.h"
#include "os.h"

#define OS_NATIVE(func) Java_org_eclipse_swt_internal_gtk_OS_##func
/* Not used */
#define OS_NATIVE_ENTER(env, that, func)
#define OS_NATIVE_EXIT(env, that, func)

#if GTK_CHECK_VERSION(3,0,0)

/* GdkEventPrivate is quite stable but sizeof GdkEvent varies */
/* struct _GdkEventPrivate_GTK_3_0 */
/* { */
  /* GdkEvent   event; */
  /* guint      flags; */
  /* void       *screen; */
  /* gpointer   windowing_data; */
  /* GdkDevice *device; */
/* }; */
#if __x86_64__
/* GdkEvent size */
/* 3-0  GdkEvent: 88 */
/* 3-4 (.0)  GdkEvent: 96 */
/* Offset for GdkEventPrivate.device: 24 */
const int _GdkEventPrivate_device_3_0 = 112;
const int _GdkEventPrivate_device_3_4 = 120;
#else
/* GdkEvent size */
/* 3-0  GdkEvent: 68 */
/* 3-4 (.0)  GdkEvent: 76 */
/* 3-18 (.0) GdkEvent: 84 */
/* Offset for GdkEventPrivate.device: 12 */
const int _GdkEventPrivate_device_3_0 = 80;
const int _GdkEventPrivate_device_3_4 = 88;
const int _GdkEventPrivate_device_3_18 = 96;
#endif

struct _GtkMenuShellPrivate_GTK_3_0
{
  void *children;
  void *parent_menu_shell;
  guint button;
  guint32 activate_time;
  guint active               : 1;
  guint have_grab            : 1;
  guint have_xgrab           : 1;
    // The rest doesn't matter
};
typedef struct _GtkMenuShellPrivate_GTK_3_0 GMSP_3_0;

struct _GtkMenuShellPrivate_GTK_3_8
{
  void *children;
  void *active_menu_item;   // 3_8
  void *parent_menu_shell;
  void *tracker;            // 3_8
  guint button;
  guint32 activate_time;
  guint active               : 1;
  guint have_grab            : 1;
  guint have_xgrab           : 1;
    // The rest doesn't matter
};
typedef struct _GtkMenuShellPrivate_GTK_3_8 GMSP_3_8;

#endif /* GTK3*/

/* {{{ XClientMessageEvent */

#ifndef NO_memmove__Lorg_eclipse_swt_internal_gtk_XClientMessageEvent_2II
JNIEXPORT void JNICALL OS_NATIVE(memmove__Lorg_eclipse_swt_internal_gtk_XClientMessageEvent_2II)
	(JNIEnv *env, jclass that, jobject arg0, jint arg1, jint arg2)
{
	XClientMessageEvent _arg0, *lparg0=NULL;
	OS_NATIVE_ENTER(env, that, memmove__Lorg_eclipse_swt_internal_gtk_XClientMessageEvent_2II_FUNC);
	if (arg0) lparg0 = &_arg0;
	memmove((void *)lparg0, (const void *)arg1, (size_t)arg2);
	if (arg0) setXClientMessageEventFields(env, arg0, lparg0);
	OS_NATIVE_EXIT(env, that, memmove__Lorg_eclipse_swt_internal_gtk_XClientMessageEvent_2II_FUNC);
}
#endif

/* }}} */

/* {{{ GdkEventCrossing */

#ifndef NO_memmove__ILorg_eclipse_swt_internal_gtk_GdkEventCrossing_2I
JNIEXPORT void JNICALL OS_NATIVE(memmove__ILorg_eclipse_swt_internal_gtk_GdkEventCrossing_2I)
	(JNIEnv *env, jclass that, jint arg0, jobject arg1, jint arg2)
{
        GdkEventCrossing _arg1, *lparg1=NULL;
	OS_NATIVE_ENTER(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventCrossing_2I_FUNC);
	if (arg1) lparg1 = getGdkEventCrossingFields(env, arg1, &_arg1);
	memmove((void *)arg0, (const void *)lparg1, (size_t)arg2);
	OS_NATIVE_EXIT(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventCrossing_2I_FUNC);
}
#endif

/* }}} */
/* {{{ GdkEventKey */

#ifndef NO_memmove__ILorg_eclipse_swt_internal_gtk_GdkEventKey_2I
JNIEXPORT void JNICALL OS_NATIVE(memmove__ILorg_eclipse_swt_internal_gtk_GdkEventKey_2I)
	(JNIEnv *env, jclass that, jint arg0, jobject arg1, jint arg2)
{
        GdkEventKey _arg1, *lparg1=NULL;
	OS_NATIVE_ENTER(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventKey_2I_FUNC);
	if (arg1) lparg1 = getGdkEventKeyFields(env, arg1, &_arg1);
	memmove((void *)arg0, (const void *)lparg1, (size_t)arg2);
	OS_NATIVE_EXIT(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventKey_2I_FUNC);
}
#endif

/* }}} */
/* {{{ GdkEventFocus */

#ifndef NO_memmove__ILorg_eclipse_swt_internal_gtk_GdkEventFocus_2I
JNIEXPORT void JNICALL OS_NATIVE(memmove__ILorg_eclipse_swt_internal_gtk_GdkEventFocus_2I)
	(JNIEnv *env, jclass that, jint arg0, jobject arg1, jint arg2)
{
        GdkEventFocus _arg1, *lparg1=NULL;
	OS_NATIVE_ENTER(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventFocus_2I_FUNC);
	if (arg1) lparg1 = getGdkEventFocusFields(env, arg1, &_arg1);
	memmove((void *)arg0, (const void *)lparg1, (size_t)arg2);
	OS_NATIVE_EXIT(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventFocus_2I_FUNC);
}
#endif

/* }}} */
/* {{{ GdkEventAny */

#ifndef NO_memmove__ILorg_eclipse_swt_internal_gtk_GdkEventAny_2I
JNIEXPORT void JNICALL OS_NATIVE(memmove__ILorg_eclipse_swt_internal_gtk_GdkEventAny_2I)
	(JNIEnv *env, jclass that, jint arg0, jobject arg1, jint arg2)
{
        GdkEventAny _arg1, *lparg1=NULL;
	OS_NATIVE_ENTER(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventAny_2I_FUNC);
	if (arg1) lparg1 = getGdkEventAnyFields(env, arg1, &_arg1);
	memmove((void *)arg0, (const void *)lparg1, (size_t)arg2);
	OS_NATIVE_EXIT(env, that, memmove__ILorg_eclipse_swt_internal_gtk_GdkEventAny_2I_FUNC);
}
#endif

/* }}} */

/* {{{ Misc */

#ifndef NO_GDK_1WINDOW_1XID
JNIEXPORT jint JNICALL OS_NATIVE(GDK_1WINDOW_1XID)
	(JNIEnv *env, jclass that, jint arg0)
{
	jint rc;
	OS_NATIVE_ENTER(env, that, GDK_1WINDOW_1XID_FUNC);
	rc = (jint)GDK_WINDOW_XID((GdkWindow *) arg0);
	OS_NATIVE_EXIT(env, that, GDK_1WINDOW_1XID_FUNC);
	return rc;
}
#endif


JNIEXPORT void JNICALL OS_NATIVE(gtk_1menu_1item_1activate)
	(JNIEnv *env, jclass that, jint arg0)
{
	OS_NATIVE_ENTER(env, that, gtk_1menu_1item_1activate);
	gtk_menu_item_activate((GtkMenuItem *) arg0);
	OS_NATIVE_EXIT(env, that, gtk_1menu_1item_1activate);
}


#if ! GTK_CHECK_VERSION(3,0,0)
JNIEXPORT void JNICALL OS_NATIVE(gtk_1item_1select)
	(JNIEnv *env, jclass that, jint arg0)
{
	OS_NATIVE_ENTER(env, that, gtk_1item_1select);
	gtk_item_select((GtkItem *) arg0);
	OS_NATIVE_EXIT(env, that, gtk_1item_1select);
}
#endif

JNIEXPORT jint JNICALL OS_NATIVE(gdk_1x11_1get_1server_1time)
	(JNIEnv *env, jclass that, jint arg0)
{
	OS_NATIVE_ENTER(env, that, gdk_1x11_1get_1server_1time);
	jint ret = gdk_x11_get_server_time((GdkWindow *) arg0);
	OS_NATIVE_EXIT(env, that, gtk_item_select);
	return ret;
}

#ifndef NO_gdk_1window_1get_1size
JNIEXPORT void JNICALL OS_NATIVE(gdk_1window_1get_1size)
	(JNIEnv *env, jclass that, jint arg0, jintArray arg1, jintArray arg2)
{
	jint *lparg1=NULL;
	jint *lparg2=NULL;
	OS_NATIVE_ENTER(env, that, gdk_1window_1get_1size_FUNC);
	if (arg1) lparg1 = (*env)->GetIntArrayElements(env, arg1, NULL);
	if (arg2) lparg2 = (*env)->GetIntArrayElements(env, arg2, NULL);
#if GTK_CHECK_VERSION(3,0,0)
	*((gint *)lparg1) = gdk_window_get_width((GdkWindow *)arg0);
	*((gint *)lparg2) = gdk_window_get_height((GdkWindow *)arg0);
# else
	gdk_window_get_size((GdkWindow *)arg0, (gint *)lparg1, (gint *)lparg2);
#endif
	if (arg2) (*env)->ReleaseIntArrayElements(env, arg2, lparg2, 0);
	if (arg1) (*env)->ReleaseIntArrayElements(env, arg1, lparg1, 0);
	OS_NATIVE_EXIT(env, that, gdk_1window_1get_1size_FUNC);
}
#endif

#ifndef NO_gtk_1dialog_1response
JNIEXPORT void JNICALL OS_NATIVE(gtk_1dialog_1response)
	(JNIEnv *env, jclass that, jint arg0, jint arg1)
{
	OS_NATIVE_ENTER(env, that, gtk_1dialog_1response_FUNC);
	gtk_dialog_response((GtkDialog *)arg0, arg1);
	OS_NATIVE_EXIT(env, that, gtk_1dialog_1response_FUNC);
}
#endif

#ifndef NO_gdk_1window_1peek_1children
JNIEXPORT jint JNICALL OS_NATIVE(gdk_1window_1peek_1children)
	(JNIEnv *env, jclass that, jint arg0)
{
	jint rc;
	OS_NATIVE_ENTER(env, that, gdk_1window_1peek_1children_FUNC);
	rc = (jint)gdk_window_peek_children((GdkWindow *)arg0);
	OS_NATIVE_EXIT(env, that, gdk_1window_1peek_1children_FUNC);
	return rc;
}
#endif

#ifndef NO__1gtk_1menu_1popup_1fake_1xgrab
JNIEXPORT void JNICALL OS_NATIVE(_1gtk_1menu_1popup_1fake_1xgrab)
	(JNIEnv *env, jclass that, jint arg0)
{
	OS_NATIVE_ENTER(env, that, _1gtk_1menu_1popup_1fake_1xgrab_FUNC);
        GtkMenu *menu = (GtkMenu*) arg0;

        GtkWidget *widget;
        GtkWidget *xgrab_shell;
        GtkWidget *parent;
        GtkMenuShell *menu_shell;

        g_return_if_fail (GTK_IS_MENU (menu));

        widget = GTK_WIDGET (menu);
        menu_shell = GTK_MENU_SHELL (menu);

#if GTK_CHECK_VERSION(3,0,0)
        if (gtk_get_minor_version() >= 8) {
            ((GMSP_3_8 *)menu_shell->priv)->parent_menu_shell = NULL;
        } else {
            ((GMSP_3_0 *)menu_shell->priv)->parent_menu_shell = NULL;
        }
#else
        menu_shell->parent_menu_shell = NULL;
#endif
        /* Find the last viewable ancestor, and fake an X grab on it */
        parent = GTK_WIDGET (menu);
        xgrab_shell = NULL;
        while (parent) {
            gboolean viewable = TRUE;
            GtkWidget *tmp = parent;

            while (tmp) {
#if GTK_CHECK_VERSION(3,0,0)
                if (!gtk_widget_get_mapped(tmp)) {
#else
                if (!GTK_WIDGET_MAPPED (tmp)) {
#endif
                    viewable = FALSE;
                    break;
                }
#if GTK_CHECK_VERSION(3,0,0)
                tmp = gtk_widget_get_parent(tmp);
#else
                tmp = tmp->parent;
#endif
            }

            if (viewable) {
                xgrab_shell = parent;
            }

#if GTK_CHECK_VERSION(3,0,0)
            parent = gtk_menu_shell_get_parent_shell(GTK_MENU_SHELL (parent));
#else
            parent = GTK_MENU_SHELL (parent)->parent_menu_shell;
#endif
        }
        if (xgrab_shell == NULL) {
            xgrab_shell = widget;
        }
#if GTK_CHECK_VERSION(3,0,0)
        if (gtk_get_minor_version() >= 8) {
            ((GMSP_3_8 *)GTK_MENU_SHELL (xgrab_shell)->priv)->have_xgrab = TRUE;
        } else {
            ((GMSP_3_0 *)GTK_MENU_SHELL (xgrab_shell)->priv)->have_xgrab = TRUE;
        }
#else
	GTK_MENU_SHELL (xgrab_shell)->have_xgrab = TRUE;
#endif

        OS_NATIVE_EXIT(env, that, _1gtk_1menu_1popup_1trick_1xgrab_FUNC);
}
#endif

#ifndef NO__1gtk_1menu_1item_1activate_1item
JNIEXPORT void JNICALL OS_NATIVE(_1gtk_1menu_1item_1activate_1item)
    (JNIEnv *env, jclass that, jint arg0)
{
	OS_NATIVE_ENTER(env, that, _1gtk_1menu_1item_1activate_1item_FUNC);
        GtkMenuItem *menu_item = (GtkMenuItem*) arg0;

        g_return_if_fail (GTK_IS_MENU_ITEM (menu_item));
        GTK_MENU_ITEM_GET_CLASS (menu_item)->activate_item(menu_item);
        OS_NATIVE_EXIT(env, that, _1gtk_1menu_1item_1activate_1item_FUNC);
}
#endif

#ifndef NO__1gtk_1menu_1shell_1activate_1item
JNIEXPORT void JNICALL OS_NATIVE(_1gtk_1menu_1shell_1activate_1item)
    (JNIEnv *env, jclass that, jint arg0, jint arg1, jboolean arg2)
{
	OS_NATIVE_ENTER(env, that, _1gtk_1menu_1shell_1activate_1item_FUNC);
        GtkMenuShell *menu_shell = (GtkMenuShell*) arg0;
        GtkMenuItem *menu_item = (GtkMenuItem*) arg1;

        g_return_if_fail (GTK_IS_MENU_SHELL (menu_shell));
        g_return_if_fail (GTK_IS_MENU_ITEM (menu_item));
        gtk_menu_shell_activate_item(GTK_MENU_SHELL(menu_shell), GTK_WIDGET(menu_item), arg2);
        OS_NATIVE_EXIT(env, that, _1gtk_1menu_1shell_1activate_1item_FUNC);
}
#endif

#ifndef NO__1gtk_1menu_1shell_1deselect
JNIEXPORT void JNICALL OS_NATIVE(_1gtk_1menu_1shell_1deselect)
    (JNIEnv *env, jclass that, jint arg0)
{
	OS_NATIVE_ENTER(env, that, _1gtk_1menu_1shell_1deselect_FUNC);
        GtkMenuShell *menu_shell = (GtkMenuShell*) arg0;

        g_return_if_fail (GTK_IS_MENU_SHELL (menu_shell));
        gtk_menu_shell_deselect(menu_shell);
        OS_NATIVE_EXIT(env, that, _1gtk_1menu_1shell_1deselect_FUNC);
}
#endif

#ifndef NO__1g_1idle_1add_1full
JNIEXPORT jint JNICALL OS_NATIVE(_1g_1idle_1add_1full)
    (JNIEnv *env, jclass that, jint arg0, jint arg1, jint arg2, jint arg3)
{
	OS_NATIVE_ENTER(env, that, _1g_1idle_1add_1full_FUNC);
        return g_idle_add_full(arg0, (GSourceFunc) arg1, (gpointer) arg2, (GDestroyNotify) arg3);
        OS_NATIVE_EXIT(env, that, _1g_1idle_1add_1full_FUNC);
}
#endif

#ifndef NO__1gdk_1threads_1add_1idle_1full
JNIEXPORT jint JNICALL OS_NATIVE(_1gdk_1threads_1add_1idle_1full)
    (JNIEnv *env, jclass that, jint arg0, jint arg1, jint arg2, jint arg3)
{
	OS_NATIVE_ENTER(env, that, _1gdk_1threads_1add_1idle_1full_FUNC);
        return gdk_threads_add_idle_full(arg0, (GSourceFunc) arg1, (gpointer) arg2, (GDestroyNotify) arg3);
        OS_NATIVE_EXIT(env, that, _1gdk_1threads_1add_1idle_1full_FUNC);
}
#endif

#ifndef NO__1gtk_1events_1pending
JNIEXPORT jint JNICALL OS_NATIVE(_1gtk_1events_1pending)
	(JNIEnv *env, jclass that)
{
	jint rc = 0;
	OS_NATIVE_ENTER(env, that, _1gtk_1events_1pending_FUNC);
	rc = (jint)gtk_events_pending();
	OS_NATIVE_EXIT(env, that, _1gtk_1events_1pending_FUNC);
	return rc;
}
#endif

/* }}} */

#if GTK_CHECK_VERSION(3,0,0)
void my_menu_position_func (GtkMenu   *menu,
                            gint      *x,
                            gint      *y,
                            gboolean  *push_in,
                            gpointer   user_data)
{
    /* fprintf(stderr, "x: %d, y: %d, user_data: %lx\n", *x, *y, (long) user_data); */
    *x = ((long) user_data) & 0xFFFF;
    *y = (((long) user_data) & 0xFFFFFFFF) >> 16;
    /* fprintf(stderr, "returning x: %d, y: %d\n", *x, *y); */
}

JNIEXPORT void JNICALL OS_NATIVE(_1gdk_1set_1event_1device_1private)
     (JNIEnv *env, jclass that, jint arg0, jint arg1)
{
    GdkEvent *event = (GdkEvent *) arg0;
    OS_NATIVE_ENTER(env, that, _1_GDK_1SET_1EVENT_1DEVICE_1PRIVATE);
    GdkDevice **device;
#if ! __x86_64__
    if (gtk_get_minor_version() >= 18) {
device = (GdkDevice **) (arg0 + _GdkEventPrivate_device_3_18);
    } else
#endif
    if (gtk_get_minor_version() >= 4) {
        device = (GdkDevice **) (arg0 + _GdkEventPrivate_device_3_4);
    } else {
        device = (GdkDevice **) (arg0 + _GdkEventPrivate_device_3_0);
    }
    *device = (GdkDevice *) arg1;
    OS_NATIVE_EXIT(env, that, _1_GDK_1SET_1EVENT_1DEVICE_1PRIVATE);
}

JNIEXPORT jint JNICALL OS_NATIVE(_1gtk_1my_1menu_1position_1func)
	(JNIEnv *env, jclass that)
{
    jint rc = (jint) &my_menu_position_func;
    return rc;
}
#endif /* GTK3 */

#ifndef NO_GTK_1WIDGET_1HEIGHT
JNIEXPORT jint JNICALL OS_NATIVE(GTK_1WIDGET_1HEIGHT)
	(JNIEnv *env, jclass that, jintLong arg0)
{
	jint rc = 0;
	OS_NATIVE_ENTER(env, that, GTK_1WIDGET_1HEIGHT_FUNC);
	rc = (jint)GTK_WIDGET_HEIGHT((GtkWidget *)arg0);
	OS_NATIVE_EXIT(env, that, GTK_1WIDGET_1HEIGHT_FUNC);
	return rc;
}
#endif


#ifndef NO_GTK_1WIDGET_1WIDTH
JNIEXPORT jint JNICALL OS_NATIVE(GTK_1WIDGET_1WIDTH)
	(JNIEnv *env, jclass that, jintLong arg0)
{
	jint rc = 0;
	OS_NATIVE_ENTER(env, that, GTK_1WIDGET_1WIDTH_FUNC);
	rc = (jint)GTK_WIDGET_WIDTH((GtkWidget *)arg0);
	OS_NATIVE_EXIT(env, that, GTK_1WIDGET_1WIDTH_FUNC);
	return rc;
}
#endif

#ifndef NO_GTK_1WIDGET_1WINDOW
JNIEXPORT jintLong JNICALL OS_NATIVE(GTK_1WIDGET_1WINDOW)
	(JNIEnv *env, jclass that, jintLong arg0)
{
	jintLong rc = 0;
	OS_NATIVE_ENTER(env, that, GTK_1WIDGET_1WINDOW_FUNC);
	rc = (jintLong)GTK_WIDGET_WINDOW((GtkWidget *)arg0);
	OS_NATIVE_EXIT(env, that, GTK_1WIDGET_1WINDOW_FUNC);
	return rc;
}
#endif

#ifndef NO_GTK_1WIDGET_1X
JNIEXPORT jint JNICALL OS_NATIVE(GTK_1WIDGET_1X)
	(JNIEnv *env, jclass that, jintLong arg0)
{
	jint rc = 0;
	OS_NATIVE_ENTER(env, that, GTK_1WIDGET_1X_FUNC);
	rc = (jint)GTK_WIDGET_X((GtkWidget *)arg0);
	OS_NATIVE_EXIT(env, that, GTK_1WIDGET_1X_FUNC);
	return rc;
}
#endif

#ifndef NO_GTK_1WIDGET_1Y
JNIEXPORT jint JNICALL OS_NATIVE(GTK_1WIDGET_1Y)
	(JNIEnv *env, jclass that, jintLong arg0)
{
	jint rc = 0;
	OS_NATIVE_ENTER(env, that, GTK_1WIDGET_1Y_FUNC);
	rc = (jint)GTK_WIDGET_Y((GtkWidget *)arg0);
	OS_NATIVE_EXIT(env, that, GTK_1WIDGET_1Y_FUNC);
	return rc;
}
#endif

#ifndef NO__1GTK_1WIDGET_1VISIBLE
JNIEXPORT jboolean JNICALL OS_NATIVE(_1GTK_1WIDGET_1VISIBLE)
	(JNIEnv *env, jclass that, jintLong arg0)
{
	jboolean rc = 0;
	OS_NATIVE_ENTER(env, that, _1GTK_1WIDGET_1VISIBLE_FUNC);
	rc = (jboolean)GTK_WIDGET_VISIBLE(arg0);
	OS_NATIVE_EXIT(env, that, _1GTK_1WIDGET_1VISIBLE_FUNC);
	return rc;
}
#endif

// Dropped in 4.8
#if ! GTK_CHECK_VERSION(3,0,0)
#ifndef NO__1gdk_1gc_1set_1clip_1region
JNIEXPORT void JNICALL OS_NATIVE(_1gdk_1gc_1set_1clip_1region)
	(JNIEnv *env, jclass that, jintLong arg0, jintLong arg1)
{
	OS_NATIVE_ENTER(env, that, _1gdk_1gc_1set_1clip_1region_FUNC);
	gdk_gc_set_clip_region((GdkGC *)arg0, (GdkRegion *) arg1);
	OS_NATIVE_EXIT(env, that, _1gdk_1gc_1set_1clip_1region_FUNC);
}
#endif
#endif

// Dropped in 4.9
#ifndef NO__1XKeysymToKeycode
JNIEXPORT jint JNICALL OS_NATIVE(_1XKeysymToKeycode)
	(JNIEnv *env, jclass that, jintLong arg0, jintLong arg1)
{
	jint rc = 0;
	OS_NATIVE_ENTER(env, that, _1XKeysymToKeycode_FUNC);
	rc = (jint)XKeysymToKeycode((Display *)arg0, (KeySym)arg1);
	OS_NATIVE_EXIT(env, that, _1XKeysymToKeycode_FUNC);
	return rc;
}
#endif

#ifndef NO__1XTestFakeKeyEvent
JNIEXPORT void JNICALL OS_NATIVE(_1XTestFakeKeyEvent)
	(JNIEnv *env, jclass that, jintLong arg0, jint arg1, jboolean arg2, jintLong arg3)
{
	OS_NATIVE_ENTER(env, that, _1XTestFakeKeyEvent_FUNC);
	XTestFakeKeyEvent((Display *)arg0, arg1, (Bool)arg2, (unsigned long)arg3);
	OS_NATIVE_EXIT(env, that, _1XTestFakeKeyEvent_FUNC);
}
#endif
