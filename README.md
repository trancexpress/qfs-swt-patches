# SWT QFS Patches

This repository contains patchfiles to modify SWT sources in a way
that an application built with it can be easily tested with (QF-Test)[https://www.qfs.de/qftest].

**If your application is built with the original swt jars distributed by
Eclipse, you do not have to patch those yourself. QF-Test automatically
instruments the required files.**

The patch-files in the repository are only required if you modify
the swt jars used by your application yourself.


## Usage

To apply the attached patches to the SWT source, place your swt sources in the folders `win32-64` and `linux-gtk-64`.

Setup example for linux:

```bash
export MYBASE=<path to your codebase SWT/src dir>
mkdir <build dir>
cd <build dir>
export BUILDDIR=`pwd`

# grep files to work on from the diff file and copy the originals to build dir
(cd $MYBASE; cp --parent `grep -- '--- a/' <diff file> | awk '{print substr ($2,24)}'` $BUILDDIR)

# apply the patch
patch -p4 < <diff file>

# in case of conflicts - fix manually...

# compile with Java 8 against your own application swt.jar and qflib.jar from .../qftest-4.7.0/qflib/qflib.jar
mkdir classes
javac -classpath swt.jar:qflib.jar -d classes `find . -name \*.java`

# create the jar
(cd classes; jar cf ../instrument.jar de org)

```

The `instrument.jar` file must be copied to the QF-Test installation
under `.../qftest-<num>/swt/linux-gtk-64/<version>`. The version
depends on the `MINOR_VERSION` constant in `org/eclipse/swt/internal/Library.java`
in your own sources. `426` equals SWT 4.12. `428` is 4.13 a.s.o, there's supposed to be an
increment of 2 for each quarterly SWT version.

The `instrument.jar` can either be used to
create an instrumented `org.eclipse.swt...jar` in the application's
plugin directory, or - newer and much more convenient - it can be used
at SUT launch time to replace the original SWT classes using a Java
agent and Byte Code Instrumentation. In either case, the original SWT
classes simply get replaced by those in the `instrument.jar`.

Besides a few changes to better enable SWT event tracking and
injection the patches include some logging calls. That's why `qflib.jar`
is needed at compile time. `qflib.jar` gets injected into the SUT in any
case.
